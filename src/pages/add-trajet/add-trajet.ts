import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FireDatabase} from "../../data/FireDatabase";

/**
 * Generated class for the AddTrajetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-trajet',
  templateUrl: 'add-trajet.html',
})
export class AddTrajetPage {
  inputD : any;
  inputA : any;
  inputHour : any;
  inputPlaces : any;
  inputPlace : any;
  inputDate : any;
  user : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public database : FireDatabase ) {
    this.user = navParams.get("username");
    console.log(this.user);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTrajetPage');
  }

  addTrip(){
    this.database.pushTrajet(this.user, this.inputD, this.inputA,this.inputDate, this.inputHour, this.inputPlaces, this.inputPlace);
    this.navCtrl.pop();
  }

}
