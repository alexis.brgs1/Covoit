import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {FireDatabase} from '../data/FireDatabase'
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule} from 'angularfire2/auth'
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../data/firebase.credentials';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DescriptionTrajetPage } from '../pages/description-trajet/description-trajet';
import { LoginPage } from '../pages/login/login';
import {File} from '@ionic-native/file';
import { AddTrajetPage } from '../pages/add-trajet/add-trajet';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    DescriptionTrajetPage,
    AddTrajetPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    DescriptionTrajetPage,
    AddTrajetPage
  ],
  providers: [
    File,
    StatusBar,
    SplashScreen,
    SQLite,
    FireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
