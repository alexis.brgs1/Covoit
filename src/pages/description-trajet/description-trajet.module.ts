import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionTrajetPage } from './description-trajet';

@NgModule({
  declarations: [
    DescriptionTrajetPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionTrajetPage),
  ],
})
export class DescriptionTrajetPageModule {}
