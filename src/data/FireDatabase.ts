import { Injectable } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth} from 'angularfire2/auth';

@Injectable()
export class FireDatabase{

    constructor(public database : AngularFireDatabase, public auth : AngularFireAuth) {
        console.log("CREATE");
    }

    authEmail(email:string, passwd : string) : Promise<firebase.auth.UserCredential>{
        return this.auth.auth.signInWithEmailAndPassword(email, passwd);
    }

    getUsers() : Promise<firebase.database.DataSnapshot>{
        return this.database.database.ref("users").once("value");
    }

    getCurrentUser() : string{
        return this.auth.auth.currentUser.email;
    }

    pushTrajet(username : String, lieuDepart : String, lieuArrivee : String, dateDepart:String, heureDebut : String, nbPsg : number, prixPlace:number){
        let trajet = {
            nameConducteur : username,
            lieuDepart : lieuDepart,
            lieuArrivee : lieuArrivee, 
            heureDebut : heureDebut,
            heureFin : "00:00",
            date : dateDepart,
            nbPassagerDispo : nbPsg,
            passagers : [],
            prixPlace : prixPlace
        }

        this.database.list('trajets').push(trajet);
    }

    getAllTrajets() : Promise<firebase.database.DataSnapshot>{
       return this.database.database.ref("trajets").once("value");
    }

    reserveTrajet(trajet : Object, username : String){
        
    }


}
