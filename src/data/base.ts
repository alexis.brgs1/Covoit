import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Injectable } from '@angular/core';

@Injectable()
export class DB{

  constructor(private sqlite: SQLite) {
    this.sqlite.create({
  name: 'data.db',
  location: 'default'
})
  .then((db: SQLiteObject) => {


    db.executeSql('create table IF NOT EXISTS MyTable(name VARCHAR(32))', {})
      .then(() => console.log('Executed SQL'))
      .catch(e => console.log(e));


  })
  .catch(e => console.log(e));
   }

   getTrajet(){
     this.sqlite.create({
   name: 'data.db',
   location: 'default'
 })
   .then((db: SQLiteObject) => {


     db.executeSql('Select name from MyTable', {})
       .then(d => console.log(d))
       .catch(e => console.log(e));


   })
   .catch(e => console.log(e));
    }
   }
