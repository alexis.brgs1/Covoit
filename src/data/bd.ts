export var Base = {
  trajets : [
    {
      idTrajet : 1,
      nameConducteur : "Simon",
      lieuDepart : "Clermont",
      lieuArrivee : "Orleans",
      heureDebut : "14:00",
      heureFin : "16:00",
      passagers : [{name:"Alexis"}],
      NombrePassagersDispo : 1,
      PrixPlace : 20
    },
    {
      idTrajet : 2,
      nameConducteur : "Simon",
      lieuDepart : "Clermont",
      lieuArrivee : "Orleans",
      heureDebut : "14:00",
      heureFin : "16:00",
      passagers : [{name:"Alexis"}],
      NombrePassagersDispo : 1,
      PrixPlace : 36
    }
  ]
}
