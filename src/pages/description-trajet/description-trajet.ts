import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Base } from "../../data/bd";
import {FireDatabase} from "../../data/FireDatabase"

/**
 * Generated class for the DescriptionTrajetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-description-trajet',
  templateUrl: 'description-trajet.html',
})
export class DescriptionTrajetPage {

  trajet : Object;
  username : String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public database : FireDatabase) {
    this.trajet = navParams.get("travel");
    this.username = navParams.get("username");
    console.log(this.trajet);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescriptionTrajetPage');
  }

  reserve(){
    this.database.reserveTrajet(this.trajet, this.username);
  }




  log(trajet){
    console.log(trajet);
  }


}
