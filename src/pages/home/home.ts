
import { Component } from '@angular/core';
import { NavController, Item } from 'ionic-angular';
import { DescriptionTrajetPage } from '../description-trajet/description-trajet';
import { FireDatabase } from "../../data/FireDatabase";
import { AngularFireDatabase } from 'angularfire2/database';
import { AddTrajetPage } from "../add-trajet/add-trajet";



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'

})
export class HomePage {


  trajets : Array<Object>;
  username : string;
  animate : String;
  site = {
    url: 'javasampleapproach.com',
    description: 'Java Technology - Spring Framework'
  };
  constructor(public navCtrl: NavController, /*public database : DB*/public db :AngularFireDatabase,public datas : FireDatabase) {
    
    // Recupération des trajets
    console.log("TEST");
    this.trajets = []
    this.datas.getAllTrajets().then((promise) => {promise.forEach(v => {this.trajets.push(v.val());})})

    this.datas.getUsers().then((promise) => {promise.forEach(v => {
      this.username = (v.val().email == this.datas.getCurrentUser()) ? v.val().username : this.username
      /*console.log(this.username)
      console.log(v.val().username)
      console.log(v.val().email)
      console.log(this.datas.getCurrentUser)
      console.log(v.val().email == this.datas.getCurrentUser())*/
    }
    )})
      
  }


  remplirListe(liste : Array<Object>){

  }
    /*this.db.list('site').push(this.site);
    this.db.database.ref('/site').on('value', function(snapshot) {
      console.log(snapshot.val());
  });
  console.log(auth.auth.signInWithEmailAndPassword("alexis@gmail.com", "blacarre"));

  
    
    
    this.Bd = Base;
    console.log("==========================");
    console.log(this.Bd);
  */

  changePage(trajet){
    
    this.navCtrl.push(DescriptionTrajetPage,
    {
      travel : trajet,
      username : this.username
    });
  }

  openAddTripForm(){
    this.navCtrl.push(AddTrajetPage,
    {
      username : this.username
    });
  }

  filters(){
    this.trajets = []
    this.datas.getAllTrajets().then((promise) => {promise.forEach(v => {
      if (v.val().nameConducteur == this.username) this.trajets.push(v.val())
    })})
  }
  
  doRefresh(refresher){
    this.trajets = []
    this.datas.getAllTrajets().then((promise) => {promise.forEach(v => {this.trajets.push(v.val())}); refresher.complete()})
  }
  

}
